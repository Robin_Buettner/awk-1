% Clear the workspace and the screen
close all;
clearvars;

%%%%%%%% SETTINGS %%%%%%%%
subject = 1; % one to four
n_tests = 1;
n_pictures = 1;
%%%%%%%%%%%%%%%%%%%%%%%%%%

%Display
ScreenSize = get(0, 'Screensize'); %Bildschirmauflösung auslesen
MaxX = ScreenSize(3); %Horizontale Bildschirmauflösung
MaxY = ScreenSize(4); %Vertikale Bildschirmauflösung
screensize = [MaxX, MaxY];

%Welcome the subject
stimfigure = figure();
ScreenSize = get(0, 'Screensize'); %Bildschirmauflösung auslesen
set(stimfigure,'MenuBar','none'); %Matlab-Menu (File, Tools etc.) verstecken
set(gcf, 'WindowState', 'fullscreen'); %Fensterdekoration ausblenden
set(stimfigure, 'Position', ScreenSize); %Figure auf den ganzen Bildschirm ausdehnen (Vollbild)
set(stimfigure, 'Color', [0 0 0]); 
axis off
set(gca,'XLim',[1 ScreenSize(3)]); %achsen so skalieren, daß 1unit = 1pixel
set(gca,'YLim',[1 ScreenSize(4)]);
set(gca,'Position',[0 0 1 1]); %gegenwärtige axis auf volle Figuregröße aufziehen  
h=text(MaxX/2,MaxY/2, sprintf('MSeKo-01 Projekt: Einflüsse auf die Auswahl interessanter Bildbereiche in natürlichen Szenen \n\n\nIm Folgenden werden 160 Bilder (in 4 Blöcken von je 40 Bildern präsentiert. \nDie fünf interessantesten Bildbereiche sollen darauf mit Klicks markiert werden. Dies sollte möglichst \nIntuitiv erfolgen, und keine lange Zeit benötigen. Nach fünf Klicks welchselt das Bild automatisch, \neine beliebig lange Pausezeit kann nach jedem Block genommen werden. \n\nBitte nehme eine Entfernung von 60cm zum Bildschirm ein (das entspricht einer Länge \nvon zwei Din-A4 Seiten) und benutze, falls nötig, deine Sehhilfe. \nVor dem Start werden die Helligkeit und Bildschirmgröße kalibriert, \nsowie mehrere Testbilder gezeigt. Der gesamte Zeitaufwand wird ca. 20 Minuten betragen. \n\nZum Fortfahren bitte eine beliebige Taste drücken...'));
set(h,'HorizontalAlignment','center');
set(h,'Fontsize',20);
set(h,'Color',[1 1 1]);
drawnow
waitforbuttonpress

% Set brightness
stimfigure = figure();
img = imread('graphics/brightness.jpg');
image(img)  %Bild Präsentieren
hold on

ScreenSize = get(0, 'Screensize');  %Bildschirmauflösung auslesen
screenCenter = [ScreenSize(3)/2 ScreenSize(4)/2];    %Koordianten des Bildschirmzentrums
MaxX = ScreenSize(3);               %Horizontale Bildschirmauflösung
MaxY = ScreenSize(4);               %Vertikale Bildschirmauflösung

set(stimfigure,'MenuBar','none');           %Matlab-Menu (File, Tools etc.) verstecken
set(stimfigure, 'WindowState', 'fullscreen');      %Fensterdekoration ausblenden
set(stimfigure, 'Color', [0 0 0]);    %Hintergrundfarbe Schwarz
set(stimfigure, 'Position', ScreenSize);    %Figure auf den ganzen Bildschirm ausdehnen (Vollbild)
axis off
waitforbuttonpress

% Adjust size if DIN-A5 sheet until satisfied
stimfigure = figure();
ScreenSize = get(0, 'Screensize'); %Bildschirmauflösung auslesen
set(stimfigure,'MenuBar','none'); %Matlab-Menu (File, Tools etc.) verstecken
set(gcf, 'WindowState', 'fullscreen'); %Fensterdekoration ausblenden
set(stimfigure, 'Position', ScreenSize); %Figure auf den ganzen Bildschirm ausdehnen (Vollbild)
set(stimfigure, 'Color', [0.6 0.6 0.6]); 
axis off
set(gca,'XLim',[1 ScreenSize(3)]); %achsen so skalieren, daß 1unit = 1pixel
set(gca,'YLim',[1 ScreenSize(4)]);
set(gca,'Position',[0 0 1 1]); %gegenwärtige axis auf volle Figuregröße aufziehen  

h=text(MaxX/2,MaxY-250,'Zur Kalibrierung bitte ein DIN-A5 Blatt querkant in den oberen Winkel halten, und danach auf die untere rechte Ecke des Blattes klicken.');
set(h,'HorizontalAlignment','center');
set(h,'Fontsize',20);
drawnow

xMin = 100;
yMax = MaxY-xMin;
linienlaenge = 400;
% upper angle
line([xMin, xMin+linienlaenge], [yMax,yMax], 'LineWidth',2);
line([xMin, xMin], [yMax,yMax-linienlaenge], 'LineWidth',2);
% click coordinate
[xClick,yClick,~] = ginput(1);
% lower angle
line([xClick, xClick], [yClick,yClick+linienlaenge], 'LineWidth',2);
line([xClick, xClick-linienlaenge], [yClick,yClick], 'LineWidth',2);
pause(0.5)

height = round(yMax - yClick);
image_size = [height, height];

%Testing
stimfigure = figure();
ScreenSize = get(0, 'Screensize'); %Bildschirmauflösung auslesen
set(stimfigure,'MenuBar','none'); %Matlab-Menu (File, Tools etc.) verstecken
set(gcf, 'WindowState', 'fullscreen'); %Fensterdekoration ausblenden
set(stimfigure, 'Position', ScreenSize); %Figure auf den ganzen Bildschirm ausdehnen (Vollbild)
set(stimfigure, 'Color', [0 0 0]); 
axis off
set(gca,'XLim',[1 ScreenSize(3)]); %achsen so skalieren, daß 1unit = 1pixel
set(gca,'YLim',[1 ScreenSize(4)]);
set(gca,'Position',[0 0 1 1]); %gegenwärtige axis auf volle Figuregröße aufziehen  
h=text(MaxX/2,MaxY/2,sprintf('Als nächstes werden Testbilder präsentiert. Bitte klicke auf die fünf interessantesten Orte im Bild. \nBitte drücke eine beliebige Taste um fortzufahren...'));
set(h,'HorizontalAlignment','center');
set(h,'Fontsize',20);
set(h,'Color',[1 1 1]);
drawnow
waitforbuttonpress

% Stimuluspräsentation über figure()
stimfigure = figure();
for i=1:n_tests
    name = sprintf('graphics/test%d.png',i);

    % Cropping the image
    img = imread(name);     
    imageSizeOriginal = size(img);
    scaling = height/imageSizeOriginal(1);
    img_resized = imresize(img, [height, imageSizeOriginal(2)*scaling]);
    imageSize = size(img_resized);

    ci = [imageSize(1)/2, imageSize(2)/2, imageSize(1)/2];     % center and radius of circle ([c_row, c_col, r])
    [xx,yy] = ndgrid((1:imageSize(1))-ci(1),(1:imageSize(2))-ci(2));
    mask = uint8((xx.^2 + yy.^2)<ci(3)^2);

    cropped = uint8(zeros(size(img_resized)));
    for j=1:3
        cropped(:,:,j) = img_resized(:,:,j).*mask;
    end

    % Display rotated and circular images on screen
    switch i
        case 1
            angle = 0;
        case 2
            angle = -45;
        case 3
            angle = -180;
    end
    
    imgRot = imrotate(cropped, angle, 'bilinear', 'crop');  %input 'crop' schneidet rotiertes Bild auf Größe von Inputbild
    image(imgRot)  %Bild Präsentieren
    hold on
    ScreenSize = get(0, 'Screensize');  %Bildschirmauflösung auslesen
    screenCenter = [ScreenSize(3)/2 ScreenSize(4)/2];    %Koordianten des Bildschirmzentrums
    MaxX = ScreenSize(3);               %Horizontale Bildschirmauflösung
    MaxY = ScreenSize(4);               %Vertikale Bildschirmauflösung

    set(stimfigure,'MenuBar','none');           %Matlab-Menu (File, Tools etc.) verstecken
    set(stimfigure, 'WindowState', 'fullscreen');      %Fensterdekoration ausblenden
    set(stimfigure, 'Position', ScreenSize);    %Figure auf den ganzen Bildschirm ausdehnen (Vollbild)
    set(stimfigure, 'Color', [0 0 0]);    %Hintergrundfarbe Schwarz
    axis off

    ySize = height;
    xSize = imageSizeOriginal(2)*scaling; %Pixel für zugehörige Bildbreite bei gegebenem Seitenverhältnis und Monitor
    xPos = MaxX/2 - xSize/2; %Position in Figure für x ...
    yPos = MaxY/2 - ySize/2; %... und y-Richtung (Position geht von der unteren linken Ecke der axis aus)
    set(gca,'unit', 'pixels', 'Position',[xPos yPos xSize ySize]);  %Positionierung & Größe der axis und damit des Bildes ändern

    % Mausklicks detektieren
    count = 0;
    while count < 5   
        [mx, my] = ginput(1); %ginput gibt Koordinaten des Klicks zurück (x,y) 
        rmx = round(mx, 0);
        rmy = round(my, 0);
        if rmx > 0 && rmy > 0 && rmx < imageSize(2) && rmy < imageSize(1)
            if mask(rmy, rmx) == 1
                plot(rmx,rmy,'r+', 'MarkerSize', 15, 'LineWidth', 1); %plot marker where mouse clicked
                count = count +1;
            end
        end
    end
    pause(0.5)
    clf
end

%End tessting
stimfigure = figure();
ScreenSize = get(0, 'Screensize'); %Bildschirmauflösung auslesen
set(stimfigure,'MenuBar','none'); %Matlab-Menu (File, Tools etc.) verstecken
set(gcf, 'WindowState', 'fullscreen'); %Fensterdekoration ausblenden
set(stimfigure, 'Position', ScreenSize); %Figure auf den ganzen Bildschirm ausdehnen (Vollbild)
set(stimfigure, 'Color', [0 0 0]); 
axis off
set(gca,'XLim',[1 ScreenSize(3)]); %achsen so skalieren, daß 1unit = 1pixel
set(gca,'YLim',[1 ScreenSize(4)]);
set(gca,'Position',[0 0 1 1]); %gegenwärtige axis auf volle Figuregröße aufziehen  
h=text(MaxX/2,MaxY/2,sprintf('Training geschafft. Lass uns mit dem ersten Block beginnen!. \nBitte drücke eine beliebige Taste um fortzufahren...'));
set(h,'HorizontalAlignment','center');
set(h,'Fontsize',20);
set(h,'Color',[1 1 1]);
drawnow
waitforbuttonpress


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Main loop for the experiment

% Create vector with image numbers for shuffeling
numbers = zeros(160,1);
orientations = zeros(160,1);
for i=1:160
    numbers(i) = i+99;
end

% Vector for the four orientations
count = 1;
for i=1:160
    orientations(i) = count;
    count = count + 1;
    if count == 5
        count = 1;
    end
end

rng(42); % Seed
numbers = numbers(randperm(length(numbers)));
orientations = orientations(randperm(length(orientations)));

switch subject
    case 1
        numbers = numbers;
        orientations = orientations;
    case 2
        numbers = numbers;
        orientations = orientations +1;
    case 3
        numbers = flipud(numbers); % Reverse vector
        orientations = flipud(orientations + 2); % Circular shift
    case 4
        numbers = flipud(numbers);
        orientations = flipud(orientations + 3);   
end   
orientations(orientations > 4) = orientations(orientations > 4) - 4; 

% Main loop
results = zeros(n_pictures, 10);
stimfigure = figure();
for i=1:n_pictures
    % Get images pseudorandomly
    name = sprintf('images/%d.png',numbers(i));

    % Cropping the image
    img = imread(name);     
    imageSizeOriginal = size(img);
    scaling = height/imageSizeOriginal(1);
    img_resized = imresize(img, [height, imageSizeOriginal(2)*scaling]);
    imageSize = size(img_resized);

    ci = [imageSize(1)/2, imageSize(2)/2, imageSize(1)/2];     % center and radius of circle ([c_row, c_col, r])
    [xx,yy] = ndgrid((1:imageSize(1))-ci(1),(1:imageSize(2))-ci(2));
    mask = uint8((xx.^2 + yy.^2)<ci(3)^2);

    cropped = uint8(zeros(size(img_resized)));
    for j=1:3
        cropped(:,:,j) = img_resized(:,:,j).*mask;
    end

    % Display rotated and circular images on screen
    switch orientations(i)
        case 1
            angle = 0;
        case 2
            angle = -45;
        case 3
            angle = -180;
        case 4
            angle = -225;
    end

    imgRot = imrotate(cropped, angle, 'bilinear', 'crop');  %input 'crop' schneidet rotiertes Bild auf Größe von Inputbild
    image(imgRot)  %Bild Präsentieren
    hold on
    ScreenSize = get(0, 'Screensize');  %Bildschirmauflösung auslesen
    screenCenter = [ScreenSize(3)/2 ScreenSize(4)/2];    %Koordianten des Bildschirmzentrums
    MaxX = ScreenSize(3);               %Horizontale Bildschirmauflösung
    MaxY = ScreenSize(4);               %Vertikale Bildschirmauflösung

    set(stimfigure,'MenuBar','none');           %Matlab-Menu (File, Tools etc.) verstecken
    set(stimfigure, 'WindowState', 'fullscreen');      %Fensterdekoration ausblenden
    set(stimfigure, 'Position', ScreenSize);    %Figure auf den ganzen Bildschirm ausdehnen (Vollbild)
    set(stimfigure, 'Color', [0 0 0]);    %Hintergrundfarbe Schwarz
    axis off

    ySize = height;
    xSize = imageSizeOriginal(2)*scaling; %Pixel für zugehörige Bildbreite bei gegebenem Seitenverhältnis und Monitor
    xPos = MaxX/2 - xSize/2; %Position in Figure für x ...
    yPos = MaxY/2 - ySize/2; %... und y-Richtung (Position geht von der unteren linken Ecke der axis aus)
    set(gca,'unit', 'pixels', 'Position',[xPos yPos xSize ySize]);  %Positionierung & Größe der axis und damit des Bildes ändern

    count = 1;
    while count < 6   
        [mx, my] = ginput(1); %ginput gibt Koordinaten des Klicks zurück (x,y) 
        rmx = round(mx, 0);
        rmy = round(my, 0);
        if rmx > 0 && rmy > 0 && rmx < imageSize(2) && rmy < imageSize(1)
            if mask(rmy, rmx) == 1
                plot(rmx,rmy,'r+', 'MarkerSize', 15, 'LineWidth', 1); %plot marker where mouse clicked
                results(i,count) = round(rmx/scaling);
                results(i,count+5) = round(rmy/scaling);
                count = count +1;
            end
        end
    end
    pause(0.3);

    if mod(i, 40) == 0
        stimfigure = figure();
        ScreenSize = get(0, 'Screensize'); %Bildschirmauflösung auslesen
        set(stimfigure,'MenuBar','none'); %Matlab-Menu (File, Tools etc.) verstecken
        set(gcf, 'WindowState', 'fullscreen'); %Fensterdekoration ausblenden
        set(stimfigure, 'Position', ScreenSize); %Figure auf den ganzen Bildschirm ausdehnen (Vollbild)
        set(stimfigure, 'Color', [0 0 0]); 
        axis off
        set(gca,'XLim',[1 ScreenSize(3)]); %achsen so skalieren, daß 1unit = 1pixel
        set(gca,'YLim',[1 ScreenSize(4)]);
        set(gca,'Position',[0 0 1 1]); %gegenwärtige axis auf volle Figuregröße aufziehen  
        h=text(MaxX/2,MaxY/2,sprint('Zeit für eine Pause? \nSobald du dich erholt hast, kannst du mit einer beliebigen Taste fortfahren...'));
        set(h,'HorizontalAlignment','center');
        set(h,'Fontsize',20);
        set(h,'Color',[1 1 1]);
        drawnow
        waitforbuttonpress
    end
    clf
end

% Save results
save(sprintf('results%d.mat', subject),'screensize', 'image_size', 'numbers','orientations','results');

% End of the experiment
stimfigure = figure();
ScreenSize = get(0, 'Screensize'); %Bildschirmauflösung auslesen
set(stimfigure,'MenuBar','none'); %Matlab-Menu (File, Tools etc.) verstecken
set(gcf, 'WindowState', 'fullscreen'); %Fensterdekoration ausblenden
set(stimfigure, 'Position', ScreenSize); %Figure auf den ganzen Bildschirm ausdehnen (Vollbild)
set(stimfigure, 'Color', [0 0 0]); 
axis off
set(gca,'XLim',[1 ScreenSize(3)]); %achsen so skalieren, daß 1unit = 1pixel
set(gca,'YLim',[1 ScreenSize(4)]);
set(gca,'Position',[0 0 1 1]); %gegenwärtige axis auf volle Figuregröße aufziehen  
h=text(MaxX/2,MaxY/2,sprintf('Geschafft! Vielen Dank für die Teilnahme. \nZum Beenden bitte eine beliebige Taste drücken...'));
set(h,'HorizontalAlignment','center');
set(h,'Fontsize',20);
set(h,'Color',[1 1 1]);
drawnow
waitforbuttonpress
close all;