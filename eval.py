import numpy as np
from scipy.io import loadmat


#fill rand coords for testing
coords_test = []
while len(coords_test) < 160:
    rand = np.random.randint(350,1350,10)
    rand = list(rand)
    coords_test.append(rand)

#merge x and y coord to tuple
def merge_coords(coords):

    merged = []
    for k in coords:
        c1 = (k[0],k[5])
        c2 = (k[1],k[6])
        c3 = (k[2],k[7])
        c4 = (k[3],k[8])
        c5 = (k[4],k[9])
        temp = [c1, c2, c3, c4, c5]
        merged.append(temp)
        
    return merged

#sort img_nr and coords into orientations
def sorting_or(orient,img,coords):
    standard, fourtyfive, fourtyfive_minus, hundredeighty = [],[],[],[]
    cnt = 0
    for x in orient:
        temp = []
        if x == 1:
            temp.append(img[cnt])
            temp.append(coords[cnt])
            standard.append(temp)
        elif x == 2:
            temp.append(img[cnt])
            temp.append(coords[cnt])
            fourtyfive.append(temp)
        elif x == 3:
            temp.append(img[cnt])
            temp.append(coords[cnt])
            fourtyfive_minus.append(temp)
        elif x == 4:
            temp.append(img[cnt])
            temp.append(coords[cnt])
            hundredeighty.append(temp)
        cnt += 1

    return standard, fourtyfive, fourtyfive_minus, hundredeighty

#sorting img_nr from 1 to 160
def bubblesort(array):
    for iter_num in range(len(array)-1,0,-1):
        for idx in range(iter_num):
            if array[idx]>array[idx+1]:
                temp = array[idx]
                array[idx] = array[idx+1]
                array[idx+1] = temp

    return array


#extract arrays from results.mat
img     = loadmat("results.mat")['numbers']
orient  = loadmat("results.mat")['orientations']
coords  = loadmat("results.mat")['results']
#convert from array[array] to array[int] structure
img = [int(x) for x in img]

'''
Muss noch "results"-ordner angelegt werden mit den 4 result-files

# sort result-files to arrays for each orientation 
# structure: [[Img1, [(x1,y1), ...]], [Img2, [(x1,y1), ...]], ...[img160, [()]]]
standard, fourtyfive, fourtyfive_minus, hundredeighty = [],[],[],[]
for res in os.listdir("results"):
    #extract arrays from results.mat
    img     = loadmat("results/%s.mat", res)['numbers']
    orient  = loadmat("results/%s.mat", res)['orientations']
    coords  = loadmat("results/%s.mat", res)['results']
    #convert from array[array] to array[int] structure
    img = [int(x) for x in img]

    #return: [[(x1,y1), ...], [..., (x5,y5)], ...]
    coords = merge_coords(coords)
    #split orientations to different arrays 
    #return: [[imgNo, [coords]], ...]
    std, ff, ffm, he = sorting_or(orient,img, ct)

    #append arrays to final orientation array until length = 160
    for i,j,k,l in zip(std,ff,ffm,he):
        standard.append(i)
        fourtyfive.append(j)
        fourtyfive_minus.append(k)
        hundredeighty.append(l)

    if len(standard) == 160:
        break

standard            = bubblesort(standard)
fourtyfive          = bubblesort(fourtyfive)
fourtyfive_minus    = bubblesort(fourtyfive_minus)
hundredeighty       = bubblesort(hundredeighty)
'''



