clearvars;
load('results1.mat'); 
% In results.results, the first five values are x coordinates and second five values are y coordinates of the clicks in ascending order

test = imread('images/172.png'); % Tested five klicks in the experiment script on the b picture
s = size(test);

for i=1:5
    for j=1:3
        test(results(i+5)-2+j, results(i)-1,:) = 0; % Make klicked pixels black
        test(results(i+5)-2+j, results(i),:) = 0;
        test(results(i+5)-2+j, results(i)+1,:) = 0;
    end
end

imshow(test); %Works perfectly