@author: Micha Burkhardt

There are two versions of the experiment:
1. experiment_psychtoolbox: Built with psychtoolbox
2. experiment_figures: Only uses MATLAB figures

At the top of the script, you need to specify your subject numer, the total number of pictures as well as your screen number 
(for the psychtoolbox one, since automatically setting it up will break on some multi-monitor settings - it will probably be 1 or 2 for you).